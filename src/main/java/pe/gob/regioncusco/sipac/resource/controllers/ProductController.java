package pe.gob.regioncusco.sipac.resource.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.regioncusco.sipac.resource.models.Product;

import java.util.Arrays;
import java.util.List;

@RestController
public class ProductController {
    @GetMapping("/products")
    public List<Product> getProducts(){
        return Arrays.asList(
                new Product[] {
                        new Product(1, "I Pas", 10),
                        new Product(1, "I Phone", 12),
                        new Product(1, "MacBook", 15)
                }
        );
    }
}
